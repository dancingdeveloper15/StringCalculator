import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringCalculatorTest {

    @Test
    @DisplayName("Add method with null param")
    void testAddWithNull() throws Exception {
        assertEquals(0, StringCalculator.add(null));
    }

    @Test
    @DisplayName("Add method with empty param")
    void testAddWithEmptyString() throws Exception {
        assertEquals(0, StringCalculator.add(""));
    }

    @Test
    @DisplayName("Add method with one number")
    void testAddWithOneNumber() throws Exception {
        assertEquals(4, StringCalculator.add("4"));
    }

    @Test
    @DisplayName("Add method with two numbers")
    void testAddWithTwoNumbers() throws Exception {
        assertEquals(9, StringCalculator.add("4,5"));
    }

    @Test
    @DisplayName("Add method with random string")
    void testAddWithRandomString()  {
        assertThrows(NumberFormatException.class, () -> StringCalculator.add("sth"));
    }

    @Test
    @DisplayName("Add method with more numbers than 2 string")
    void testAddWithMoreNumbers() throws Exception {
        assertEquals(20, StringCalculator.add("3,8,2,7"));
    }

    @Test
    @DisplayName("Add method with error in String")
    void testAddWithErrorInString() {
        assertThrows(NumberFormatException.class, () -> StringCalculator.add("3,8 ,2,7"));
    }

    @Test
    @DisplayName("Add method with new lines")
    void testAddWithNewLines() throws Exception {
        assertEquals(20, StringCalculator.add("3\n8\n2\n7"));
    }

    @Test
    @DisplayName("Add method with error in String with new lines")
    void testAddWithErrorInStringWithNewLines() {
        assertThrows(NumberFormatException.class, () -> StringCalculator.add("3\n8 \n2\n7"));
    }

    @Test
    @DisplayName("Add method with custom delimiters")
    void testAddWithCustomDelimiter() throws Exception {
        assertEquals(20, StringCalculator.add("//[*]\n3*8*2*7"));
    }

    @Test
    @DisplayName("Add method with error in delimiter pattern")
    void testAddWithErrorInDelimiterPattern() {
        assertThrows(NumberFormatException.class, () -> StringCalculator.add("//[*] \n3*8*2*7"));
    }

    @Test
    @DisplayName("Add method with error caused by negative numbers")
    void testAddWithNegativeNumbers() {
        Exception thrown = assertThrows(Exception.class, () -> StringCalculator.add("//[*]\n3*-8*2*-7"));
        assertEquals("negatives not allowed [-8, -7]", thrown.getMessage());
    }

    @Test
    @DisplayName("Add method with bigger number than 1000")
    void testAddWithBiggerNumberThan1000() throws Exception{
        assertEquals(20, StringCalculator.add("3,8,2,7,1001"));
    }

    @Test
    @DisplayName("Add method with bigger number than 1000 with custom delimeter")
    void testAddWithBiggerNumberThan1000WithCustomDelimeter() throws Exception{
        assertEquals(20, StringCalculator.add("//[*]\n3*8*2*7*1001"));
    }

    @Test
    @DisplayName("Add method with bigger number than 1000 with new lines")
    void testAddWithBiggerNumberThan1000WithNewLines() throws Exception {
        assertEquals(20, StringCalculator.add("3\n8\n2\n7\n1001"));
    }

    @Test
    @DisplayName("Add method with delimiter longer than 1 char")
    void testAddWithDelimiterWithMoreThanOneChar1() throws Exception{
        assertEquals(20, StringCalculator.add("//[*\n!]\n3*\n!8*\n!2*\n!7"));
    }

    @Test
    @DisplayName("Add method with delimiter longer than 1 char 2 ")
    void testAddWithDelimiterWithMoreThanOneChar2() throws Exception{
        assertEquals(20, StringCalculator.add("//[*********]\n3*********8*********2*********7"));
    }

    @Test
    @DisplayName("Add method with multiple delimiters ")
    void testAddWithMultipleDelimiters() throws Exception{
        assertEquals(20, StringCalculator.add("//[*][!]\n3!8!2*7"));
    }

    @Test
    @DisplayName("Add method with multiple delimiters longer than 1 char")
    void testAddWithMultipleDelimitersWithMoreThanOneChar() throws Exception{
        assertEquals(20, StringCalculator.add("//[***][&%!]\n3***8***2&%!7"));
    }

    @Test
    @DisplayName("Add method with multiple delimiters with Error in string format")
    void testAddWithMultipleDelimitersWithErrorInFormat(){
        assertThrows(NumberFormatException.class, () -> StringCalculator.add("//[***][&%!]\n3***8** *2&%!7"));
    }
}