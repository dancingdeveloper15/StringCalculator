import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

public class StringCalculator {

    public static int add(String numbers) throws Exception {
        if (checkStringEmpty(numbers)) {
            return 0;
        }
        numbers = supportCustomDelimiters(numbers);
        numbers = replaceNewLines(numbers);
        if (numbers.contains(",")) {
            return addCommaSeparatedNumbers(numbers);
        } else {
            return Integer.parseInt(numbers);
        }
    }

    private static boolean checkStringEmpty(String number) {
        return number == null || number.isEmpty();
    }

    private static String replaceNewLines(String numbers) {
        if (numbers.contains("\n")) {
            return numbers.replace("\n", ",");
        }
        return numbers;
    }

    private static String supportCustomDelimiters(String numbers) {
        if (numbers.length() > 6 && "//".equals(numbers.substring(0,2))) {
            numbers = numbers.replaceFirst("//", "");
            List<String> delimiterList = new ArrayList<>();
            while("[".equals(numbers.substring(0,1))) {
                delimiterList.add(numbers.substring(numbers.indexOf("[") + 1, numbers.indexOf("]")));
                numbers = numbers.substring(numbers.indexOf("]") + 1);
            }
            numbers = numbers.replaceFirst("\n", "");
            for(String delimiter: delimiterList){
                numbers =  numbers.replace(delimiter, ",");
            }
            return numbers;
        }
        return numbers;
    }

    private static int addCommaSeparatedNumbers(String numbers) throws Exception{
        int result = 0;
        List<Integer> negativeNumbers = new ArrayList<>();
        String[] strNumbersArray = numbers.split(",");
        for (String strNumb : strNumbersArray) {
            int number = 0;
            if(!checkStringEmpty(strNumb)){
                number = Integer.parseInt(strNumb);
            }
            if(number < 0){
                negativeNumbers.add(number);
            } else if(number <= 1000){
                result = Math.addExact(result, number);
            }
        }
        if(negativeNumbers.size() > 0) {
            throw new Exception( String.format("negatives not allowed %s", negativeNumbers));
        }
        return result;
    }
}

